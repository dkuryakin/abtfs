cd "$(dirname "$0")"
cd ..
rm -rf ABTFS_RELEASE
mkdir ABTFS_RELEASE
cd ABTFS_RELEASE
cmake ../ABTFS
make
cp abtfs ../ABTFS
cd ..
rm -rf ABTFS_RELEASE
