#pragma once

////////////////////////////////////////////////////////////////////////////////

#include <common/common.h>
#include <vector>
#include <list>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {
namespace NAnimat {

////////////////////////////////////////////////////////////////////////////////

class TGNG : public TBaseAnimat<TFloatVector, TFloatVector> {
public:
    typedef TBaseAnimat<TFloatVector, TFloatVector> TParent;
    typedef TParent::TSensation TSensation;
    typedef TParent::TAction TAction;
    typedef TGNG TSelf;

    ////////////////////////////////////////////////////////////////////////////

private:
    struct TNode;
    struct TEdge;

    typedef std::list<TNode*> TNList;
    typedef std::list<TEdge*> TEList;

    ////////////////////////////////////////////////////////////////////////////

    struct TNode {
        TSensation Sensation;

        TEList OutputEdges;
        TEList InputEdges;

        ui_t Depression;
        float_t iR;

        float_t he;
        float_t fe;
        float_t ce;

        float_t hi;
        float_t fi;
        float_t ci;

        float_t _hi;
        float_t _fi;
        float_t _ci;

        float_t NuI;

        ui_t DT;

        TNode(const TSensation& s) : Sensation(s), Depression(0), iR(0.0),
        he(0.0), fe(0.0), ce(0.0), hi(0.0), fi(0.0), ci(0.0),
        _hi(0.0), _fi(0.0), _ci(0.0), NuI(0.0), DT(0)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////

    struct TEdge {
        TAction Action;
        
        TNode* From;
        TNode* To;

        ui_t Exp;
        ui_t Age;
        ui_t Depression;
        bool Desired;

        TEdge(const TAction& a = TAction(),
              TNode* from = 0,
              TNode* to = 0) :
        Action(a), From(from), To(to), Exp(0), Age(0), Depression(0), 
        Desired(false) {}
    };

    ////////////////////////////////////////////////////////////////////////////

    TNList Nodes;
    TEList Edges;

    TNode* W1;
    TNode* W2;
    mutable TNode* ExpectedW1;
    bool IsFirstStep;

    TNList FoodNodes;
    TSensation CurrSens;
    mutable TAction LastAction;

    ////////////////////////////////////////////////////////////////////////////

    float_t Error;
    ui_t Steps;
    bool IsFood;

    float_t Hung;
    float_t Fear;
    float_t Curi;

    float_t Ksi; // Security drive

    ////////////////////////////////////////////////////////////////////////////

    // TGNG consts
    const float_t KsiEps;
    const float_t StepLength;
    const float_t ActionSigma;
    const float_t Delta;
    const float_t BetaM;
    const float_t EpsW;
    const ui_t KsiA;
    const ui_t iE;
    const ui_t iN;

    ////////////////////////////////////////////////////////////////////////////

    // Motivational module constants
    const float_t Gamma;
    const float_t NuH;
    const float_t DKsi;
    const float_t RoI;
    const float_t FiKsi;
    const float_t NuF;
    const float_t BetaC;
    const ui_t NC;
    const ui_t FiC;
    const float_t NuC;
    const float_t PH;
    const float_t PF;
    const float_t PC;
    const float_t BetaA;

    ////////////////////////////////////////////////////////////////////////////

    NGraphics::TPlot<size_t, float> PlotH;
    NGraphics::TPlot<size_t, float> PlotF;
    NGraphics::TPlot<size_t, float> PlotC;

    NGraphics::TPlot<size_t, float> PlotHI;
    NGraphics::TPlot<size_t, float> PlotFI;
    NGraphics::TPlot<size_t, float> PlotCI;

    NGraphics::TPlot<size_t, float> PlotHE;
    NGraphics::TPlot<size_t, float> PlotFE;
    NGraphics::TPlot<size_t, float> PlotCE;

    NGraphics::TPlot<size_t, float> PlotWH;
    NGraphics::TPlot<size_t, float> PlotWF;
    NGraphics::TPlot<size_t, float> PlotWC;

    NGraphics::TPlot<size_t, float> PlotNuI;

    ////////////////////////////////////////////////////////////////////////////

public:
    TGNG(float_t ksiEps = 5.0, // 2.0 || 5.0
         float_t stepLength = 2.0,  // 1.0
         float_t actionSigma = 0.0, // 0.1
         float_t delta = 0.5,
         float_t betaM = 0.1,
         float_t epsW = 0.05,
         ui_t ksiA = 100, // 100
         ui_t ie = 100,
         ui_t in = 10, 
         float_t gamma = 0.99,
         float_t nuH = 0.002,
         float_t dKsi = 0.0045, // 0.0045
         float_t roI = 0.99995, // 0.99995
         float_t fiKsi = 0.5,
         float_t nuF = 0.01, 
         float_t betaC = 0.5, // 0.5
         ui_t nC = 8,
         ui_t fiC = 1,
         float_t nuC = 0.001,
         float_t pH = 1.0 * 1, // 1.0
         float_t pF = 0.5 * 1, // 0.5
         float_t pC = 0.1 * 1, // 0.1
         float_t betaA = 10.0) : 
    W1(0),
    W2(0),
    ExpectedW1(0),
    IsFirstStep(true),
    Error(0.0),
    Steps(0),
    IsFood(false),
    Hung(0.0),
    Fear(0.0),
    Curi(0.0),
    Ksi(0.0),
    KsiEps(ksiEps),
    StepLength(stepLength),
    ActionSigma(actionSigma),
    Delta(delta),
    BetaM(betaM),
    EpsW(epsW),
    KsiA(ksiA),
    iE(ie),
    iN(in),
    Gamma(gamma),
    NuH(nuH),
    DKsi(dKsi),
    RoI(roI),
    FiKsi(fiKsi),
    NuF(nuF),
    BetaC(betaC),
    NC(nC),
    FiC(fiC),
    NuC(nuC),
    PH(pH),
    PF(pF),
    PC(pC),
    BetaA(betaA),
    PlotH(1, 0.0, 1.0, 500, 650, 0, 500, 100, NGraphics::clr_RED),
    PlotF(1, 0.0, 1.0, 500, 650, 0, 500, 100, NGraphics::clr_GREEN),
    PlotC(1, 0.0, 1.0, 500, 650, 0, 500, 100, NGraphics::clr_BLUE),
    PlotHI(1, 0.0, 1.0, 500, 650, 110, 500, 100, NGraphics::clr_RED),
    PlotFI(1, 0.0, 1.0, 500, 650, 110, 500, 100, NGraphics::clr_GREEN),
    PlotCI(1, 0.0, 1.0, 500, 650, 110, 500, 100, NGraphics::clr_BLUE),
    PlotHE(1, 0.0, 1.0, 500, 650, 220, 500, 100, NGraphics::clr_RED),
    PlotFE(1, 0.0, 1.0, 500, 650, 220, 500, 100, NGraphics::clr_GREEN),
    PlotCE(1, 0.0, 1.0, 500, 650, 220, 500, 100, NGraphics::clr_BLUE),
    PlotWH(1, 0.0, 1.0, 500, 650, 330, 500, 100, NGraphics::clr_RED),
    PlotWF(1, 0.0, 1.0, 500, 650, 330, 500, 100, NGraphics::clr_GREEN),
    PlotWC(1, 0.0, 1.0, 500, 650, 330, 500, 100, NGraphics::clr_BLUE),
    PlotNuI(1, 0.0, 1.0, 500, 650, 440, 500, 100, NGraphics::clr_RED)
    {
        LastAction = RandomAction();
    }

    ////////////////////////////////////////////////////////////////////////////

#define FOR_EACH(IT, LIST, TYPE) typename TYPE::iterator IT = LIST.begin(); \
    for (; IT != LIST.end(); ++IT)

    ////////////////////////////////////////////////////////////////////////////

    virtual ~TGNG() {
        FOR_EACH(it, Nodes, TNList)
            delete *it;
        FOR_EACH(eit, Edges, TEList)
            delete *eit;
    }

    ////////////////////////////////////////////////////////////////////////////

private:

    ////////////////////////////////////////////////////////////////////////////

    TNode* WinnerNode(const TSensation& sensation) {
        TNode* node = 0;
        float_t minDist = MAX_FLOAT;
        FOR_EACH(it, Nodes, TNList) {
            float_t dist = norm_2((*it)->Sensation - sensation);
            if (dist < minDist) {
                minDist = dist;
                node = *it;
            }
        }
        return node;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void UpdateError() {
        Error = Error * (1.0 - Delta) + 
            Delta * norm_2(CurrSens - W1->Sensation);
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void UpdateEdge(TEdge* edge, const TAction& act) {
        if (edge->Exp * BetaM < 1.0)
            edge->Action = (edge->Exp * edge->Action + act) / (edge->Exp + 1);
        else
            edge->Action = (edge->Action + act * BetaM) / (1.0 + BetaM);
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void UpdateNode(TNode* node, const TSensation& sens) {
        node->Sensation = (1.0 - EpsW) * node->Sensation + EpsW * sens;
    }    

    ////////////////////////////////////////////////////////////////////////////

    TAction RandomAction() const {
        TAction a(2);
        float_t alpha = Uniform<float_t>(0.0, 2 * PI);
        a[0] = cos(alpha);
        a[1] = sin(alpha);
        return a * StepLength;
    }

    ////////////////////////////////////////////////////////////////////////////

    TAction GetOptimalAction() const {
        TAction a = RandomAction(); // TODO: RandomAction() || LastAction ? ;
        //TAction a = (Uniform(0.0, 10.0) < 1.0) ? RandomAction() : LastAction;
        ExpectedW1 = 0;
        float_t nui = 0.0;
        FOR_EACH(eit, W1->OutputEdges, TEList) {
            TNode* n = (*eit)->To;
            if (n->NuI > nui && W1->NuI < n->NuI) {
                nui = n->NuI;
                a = (*eit)->Action;
                ExpectedW1 = (*eit)->To;
            }
        }

        // TODO: Don't know if it's correct to use log epsilon-rule here?
        //if (Uniform(0.0, log(Steps + 1)) < 1.0)
        //    a = RandomAction();

        return (StepLength / norm_2(a)) * a;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void FirstStep() {
        if (!IsFirstStep)
            return;
        TNode* node = new TNode(CurrSens);
        W1 = W2 = node;
        Nodes.push_back(node);
        IsFirstStep = false;
    }

    ////////////////////////////////////////////////////////////////////////////

    TEdge* Connect(TNode* from, TNode* to, const TAction& a) {
        TEdge* edge = new TEdge(a, from, to);
        from->OutputEdges.push_back(edge);
        to->InputEdges.push_back(edge);
        Edges.push_back(edge);
        return edge;
    }

    ////////////////////////////////////////////////////////////////////////////

    void Disconnect(TNode* from, TNode* to) {
        TEdge* edge = GetConnection(from, to);
        RemoveEdge(Edges, edge);
        RemoveEdge(from->OutputEdges, edge);
        RemoveEdge(to->InputEdges, edge);
        delete edge;
    }

    ////////////////////////////////////////////////////////////////////////////

    TNode* CreateNode(const TSensation& s, const TAction& a, TNode* from) {
        TNode* node = new TNode(s);
        Nodes.push_back(node);
        Connect(from, node, a);
        return node;
    }

    ////////////////////////////////////////////////////////////////////////////

    TEdge* GetConnection(TNode* from, TNode* to) {
        FOR_EACH(eit, from->OutputEdges, TEList) {
            if ((*eit)->To == to)
                return *eit;
        }
        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////

    void IncrementAges(TNode* node) {
        FOR_EACH(eit, node->OutputEdges, TEList) {
            (*eit)->Age++;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    void RemoveEdge(TEList& elist, TEdge* edge) {
        FOR_EACH(eit, elist, TEList) {
            if (*eit == edge) {
                elist.erase(eit);
                return;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////

#define FOR_EACH_WO_INC(IT, LIST, TYPE) typename TYPE::iterator IT = LIST.begin(); \
    for (; IT != LIST.end();)

    ////////////////////////////////////////////////////////////////////////////

    void CleanEdges() {
        FOR_EACH_WO_INC(eit, Edges, TEList) {
            if ((*eit)->Age > KsiA) {
                RemoveEdge((*eit)->From->OutputEdges, *eit);
                RemoveEdge((*eit)->To->InputEdges, *eit);
                delete *eit;
                eit = Edges.erase(eit);
            } else
                ++eit;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    void CleanNodes() {
        FOR_EACH_WO_INC(it, Nodes, TNList) {
            if ((*it)->OutputEdges.empty()) {
                FOR_EACH(eit, (*it)->InputEdges, TEList) {
                    RemoveEdge(Edges, *eit);
                    delete *eit;
                }
                delete *it;
                it = Nodes.erase(it);
            } else
                ++it;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void Clean() {
        CleanEdges();
        CleanNodes();
    }

    ////////////////////////////////////////////////////////////////////////////

    void UpdateDepressions() {
        FOR_EACH(eit, Edges, TEList) {
            if ((*eit)->Depression > 0)
                (*eit)->Depression--;
        }
        FOR_EACH(it, Nodes, TNList) {
            if ((*it)->Depression > 0)
                (*it)->Depression--;
        }        
    }

    ////////////////////////////////////////////////////////////////////////////

    void UpdateNodesAges() {
        FOR_EACH(it, Nodes, TNList) {
            (*it)->DT++;
        }        
    }

    ////////////////////////////////////////////////////////////////////////////

    void SplitSensation(const TSensation& sensation) {
        IsFood = sensation[sensation.size() - 1];
        CurrSens = sensation;
        CurrSens.resize(CurrSens.size() - 1);
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void DecreaseHunger() {
        Hung = Max(Hung - NuH, 0.0);
    }

    ////////////////////////////////////////////////////////////////////////////

    float_t DistMin(const TSensation& s) {
        float_t distMin = MAX_FLOAT;
        for (ui_t i = 0; i < s.size(); ++i) {
            float_t dist = 0.0;
            for (ui_t k = 0; k < 5; ++k)
                dist += s[(i + k) % s.size()];
            if (dist < distMin)
                distMin = dist;
        }
        return distMin;
    }

    ////////////////////////////////////////////////////////////////////////////

    template <class C, class V>
    bool Contains(const C& c, V v) {
        typename C::const_iterator it = c.begin();
        for (; it != c.end(); ++it) {
            if (*it == v)
                return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////

    void DecreaseIR() {
        FOR_EACH(it, FoodNodes, TNList) {
            (*it)->iR *= RoI;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    inline float_t SeqLevel(const TSensation& s) {
        float_t x = DistMin(s) * DKsi;
        return Max(1.0 - x, 0.0);     
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void UpdateKsi() {
        Ksi = SeqLevel(CurrSens); 
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void DecreaseFear() {
        Fear += (Ksi - FiKsi) * NuF;
        Fear = Max(0.0, Fear);
        Fear = Min(1.0, Fear);
    }

    ////////////////////////////////////////////////////////////////////////////

    void UpdateExternals() {
        // Food
        FOR_EACH(itH, FoodNodes, TNList) {
            (*itH)->he = 1.0 - (*itH)->iR;
        }

        // Fear
        FOR_EACH(itF, Nodes, TNList) {
            (*itF)->fe = SeqLevel((*itF)->Sensation);
        }

        // Curiocity
        FOR_EACH(itC, Nodes, TNList) {
            ui_t SI = 0;
            ui_t CN = 0;
            FOR_EACH(eit, (*itC)->OutputEdges, TEList) {
                ui_t e = (*eit)->Exp;
                SI += (e < NC) ? (NC - e) : 0;
                CN += NC;
            }
            (*itC)->ce = BetaC * (1.0 - exp(-NuC * (*itC)->DT)) +
                (1.0 - BetaC) * SI / CN /* (NC * FiC) */; // TODO: It's seems
                                                          // more correct.
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    inline float_t w(float_t sigm, float_t p) {
        return p / (1.0 + exp(-BetaA * (0.5 - sigm)));
    }

    ////////////////////////////////////////////////////////////////////////////

    void UpdateInternals() {
        FOR_EACH(it, Nodes, TNList) {
            // Food
            float_t hin = 0.0;
            FOR_EACH(eitH, (*it)->OutputEdges, TEList) {
                if ((*eitH)->Depression == 0) {
                    TNode* n = (*eitH)->To;
                    float_t hinc = n->hi + w(Fear, PF) * (n->fi - 1);
                    if (hinc > hin)
                        hin = hinc;
                }
            }
            hin = hin * Gamma;
            (*it)->_hi = (hin > (*it)->he) ? hin : (*it)->he;

            // Fear
            float_t fin = 0.0;
            FOR_EACH(eitF, (*it)->OutputEdges, TEList) {
                if ((*eitF)->Depression == 0) {
                    TNode* n = (*eitF)->To;
                    float_t finc = n->fi;
                    if (finc > fin)
                        fin = finc;
                }
            }
            fin = fin * Gamma;
            (*it)->_fi = (fin > (*it)->fe) ? fin : (*it)->fe;

            // Curiocity
            float_t cin = 0.0;
            FOR_EACH(eitC, (*it)->OutputEdges, TEList) {
                if ((*eitC)->Depression == 0) {
                    TNode* n = (*eitC)->To;
                    float_t cinc = n->ci;
                    if (cinc > cin)
                        cin = cinc;
                }
            }
            cin = cin * Gamma;
            (*it)->_ci = (cin > (*it)->ce) ? cin : (*it)->ce;
        }
        FOR_EACH(nit, Nodes, TNList) {
            (*nit)->hi = (*nit)->_hi;
            (*nit)->fi = (*nit)->_fi;
            (*nit)->ci = (*nit)->_ci;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    void CombineInternals() {
        FOR_EACH(it, Nodes, TNList) {
            float_t wh = w(Hung, PH);
            float_t wf = w(Fear, PF);
            float_t wc = w(Curi, PC);
            float_t h = (*it)->hi;
            float_t f = (*it)->fi;
            float_t c = (*it)->ci;
            float_t depr = static_cast<float_t>(iN - (*it)->Depression) / iN;
            (*it)->NuI = depr * (wh * h + wf * f + wc * c) / (wh + wf + wc);
        }        
    }

    ////////////////////////////////////////////////////////////////////////////

    void UpdateMotivations() {
        if (IsFood) {
            Hung = 1.0;
            if (Contains(FoodNodes, W1))
                W1->iR = 1.0;
            else {
                FoodNodes.push_back(W1);
                W1->iR = 1.0;
            }
        } else {
            DecreaseHunger();
            if (Contains(FoodNodes, W1))
                W1->iR = 1.0;
        }
        DecreaseIR();
        UpdateKsi();
        DecreaseFear(); // TODO: ????? don't know if it is correct place
        UpdateExternals();
        UpdateInternals();
        CombineInternals();
    }

    ////////////////////////////////////////////////////////////////////////////

public:
    virtual TAction GetAction() const {
        LastAction = GetOptimalAction();
        return NormalVector(LastAction, ActionSigma);
    }

    ////////////////////////////////////////////////////////////////////////////

    virtual void ApplySensation(const TSensation& sensation) {
        UpdateDepressions();
        UpdateNodesAges();

        SplitSensation(sensation);
        FirstStep();
        W2 = W1;
        W1 = WinnerNode(CurrSens);
        UpdateError();
        if (Error > KsiEps) {
            // TODO: ????????? Add movement LastAction to histogram of node W2
            W1 = CreateNode(CurrSens, LastAction, W2);
            Error = 0.0;
        } else if (W1 != W2) {
            // TODO: ?????????? Add movement LastAction to histogram of node W2
            TEdge* edge = GetConnection(W2, W1);
            if (edge) {
                edge->Age = 0;
                edge->Exp++;
                UpdateEdge(edge, LastAction);
            } else
                edge = Connect(W2, W1, LastAction);

            UpdateNode(W1, CurrSens);
            IncrementAges(W1);
            Clean();
            // TODO: Do something with following code:
            //if (ExpectedW1 == 0 || ExpectedW1 != W1)
            //    edge->Depression = iE;
            W2->Depression = iN; 
        }
        W1->DT = 0;
        UpdateMotivations();
        ++Steps;

        // Following code is used just for visualization
        PlotH.PushPoint(Hung);
        PlotF.PushPoint(Fear);
        PlotC.PushPoint(Curi);
        PlotHI.PushPoint(W1->hi);
        PlotFI.PushPoint(W1->fi);
        PlotCI.PushPoint(W1->ci);
        PlotHE.PushPoint(W1->he);
        PlotFE.PushPoint(W1->fe);
        PlotCE.PushPoint(W1->ce);
        PlotWH.PushPoint(w(Hung, PH));
        PlotWF.PushPoint(w(Fear, PF));
        PlotWC.PushPoint(w(Curi, PC));
        PlotNuI.PushPoint(W1->NuI);
    }

    ////////////////////////////////////////////////////////////////////////////

    virtual void Visualize() {
        PlotH.Plot();
        PlotF.Plot();
        PlotC.Plot();
        PlotHI.Plot();
        PlotFI.Plot();
        PlotCI.Plot();
        PlotHE.Plot();
        PlotFE.Plot();
        PlotCE.Plot();
        PlotWH.Plot();
        PlotWF.Plot();
        PlotWC.Plot();
        PlotNuI.Plot();
        std::cerr << "Steps=" << Steps 
                  << ", Nodes=" << Nodes.size()
                  << ", Edges=" << Edges.size()
                  << ", Ksi=" << Ksi
                  << ", Fear=" << Fear
                  << ", W1->hi=" << W1->hi
                  << ", W1->fi=" << W1->fi
                  << ", W1->ci=" << W1->ci
                  << ", W1->he=" << W1->he
                  << ", W1->fe=" << W1->fe
                  << ", W1->ce=" << W1->ce
                  << ", W1->NuI=" << W1->NuI
                  << ", FoodNodes=" << FoodNodes.size()
                  << ", Hung=" << Hung
                  << std::endl;

        FOR_EACH(it, FoodNodes, TNList) {
            std::cerr << "Food[i]->he=" << (*it)->he
                      << ", Food[i]->hi=" << (*it)->hi
                      << ", Food[i]->_hi=" << (*it)->_hi
                      << ", Food[i]->X1=" << (*it)->Sensation[0]
                      << ", Food[i]->X2=" << (*it)->Sensation[1]
                      << ", Food[i]->X3=" << (*it)->Sensation[2]
                      << ", Food[i]->X4=" << (*it)->Sensation[3]
                      << ", Food[i]->X5=" << (*it)->Sensation[4]
                      << ", Food[i]->X6=" << (*it)->Sensation[5]
                      << ", Food[i]->X7=" << (*it)->Sensation[6]
                      << ", Food[i]->X8=" << (*it)->Sensation[7]
                      << std::endl;
        }

        ui_t maxAge = 0;
        FOR_EACH(eit, Edges, TEList) {
            if ((*eit)->Age > maxAge)
                maxAge = (*eit)->Age;
        }
        std::cerr << "### MAX_AGE = " << maxAge
                  << std::endl;
    }
};

////////////////////////////////////////////////////////////////////////////////

}} // NABTFS::NAnimat
