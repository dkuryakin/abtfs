#include <environments/environments.h>
#include <animats/animats.h>
#include <common/common.h>
#include <experiments/experiments.h>

#include <sstream>

////////////////////////////////////////////////////////////////////////////////

using namespace NABTFS::NEnvironment;
using namespace NABTFS::NAnimat;
using namespace NABTFS::NInteractor;
using namespace NABTFS::NGraphics;
using namespace NABTFS;

////////////////////////////////////////////////////////////////////////////////

typedef TMaze2D TEnv;
typedef TGNG TAnm;
typedef TInteractor<TFloatVector, TFloatVector> TInt;

////////////////////////////////////////////////////////////////////////////////

template <class I, class O>
O Convert(I i) {
    std::stringstream ss;
    ss << i;
    O o;
    ss >> o;
    return o;
}

int main(int argc, char* argv[]) {
    if (argc == 3) {
        TEnv* maze = TEnv::LoadMaze(argv[1]);    
        TAnm* anm = new TAnm;
        TInt intr(maze, anm, 50);
        ui_t steps = Convert<const char*, ui_t>(argv[2]);
        for (ui_t i = 0; i < steps ; ++i) {
            intr.NextStep();
            //Delay(10);
        }
    
        delete maze;
        delete anm;
    } else if (argc == 7) {
        const char* maze_1 = argv[1];
        ui_t steps_1 = Convert<const char*, ui_t>(argv[2]);
        const char* maze_2 = argv[3];
        ui_t steps_2 = Convert<const char*, ui_t>(argv[4]);
        const char* maze_3 = argv[5];
        ui_t steps_3 = Convert<const char*, ui_t>(argv[6]);
        NABTFS::N_MAZE2D_TGNG::DoExperiment(maze_1, steps_1, maze_2, steps_2, 
            maze_3, steps_3, 50);
    }
    return 0;
}
