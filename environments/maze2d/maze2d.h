#pragma once

////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>
#include <common/common.h>

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {
namespace NEnvironment {

////////////////////////////////////////////////////////////////////////////////

class TMaze2D : public TBaseEnvironment<TFloatVector, TFloatVector> {
public:
    typedef TBaseEnvironment<TFloatVector, TFloatVector> TParent;
    typedef TParent::TSensation TSensation;
    typedef TParent::TAction TAction;
    typedef TMaze2D TSelf;

    ////////////////////////////////////////////////////////////////////////////

private:
    typedef TFloatVector TPoint;

    struct TFood {
        TPoint Position;
        ui_t Counter;
    };

    typedef std::list<TPoint> TPoints;
    typedef std::vector<TFood> TFoods;
    typedef TUIntMatrix TMaze;

    ////////////////////////////////////////////////////////////////////////////

    const ui_t SizeX;
    const ui_t SizeY;
    const float_t BoxSizeX;
    const float_t BoxSizeY;
    const float_t FoodRadius;
    const ui_t FoodCounter;

    const ui_t StoreSteps;
    const float_t SensSigma;

    ////////////////////////////////////////////////////////////////////////////

    TPoint Position;
    TFoods Foods;
    TMaze Maze;
    float_t Food;

    TPoints Points;

    ////////////////////////////////////////////////////////////////////////////

    inline bool IsWall(const TPoint& p) const {
        ui_t x = static_cast<ui_t>(p[0] / BoxSizeX);
        ui_t y = static_cast<ui_t>(p[1] / BoxSizeY);
        return Maze(x, y) == 1;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline bool IsTeleport(const TPoint& p) const {
        ui_t x = static_cast<ui_t>(p[0] / BoxSizeX);
        ui_t y = static_cast<ui_t>(p[1] / BoxSizeY);
        return Maze(x, y) > 1;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline TPoint Teleportation(const TPoint& p) const {
        ui_t x = static_cast<ui_t>(p[0] / BoxSizeX);
        ui_t y = static_cast<ui_t>(p[1] / BoxSizeY);
        float_t dx = p[0] - x * BoxSizeX;
        float_t dy = p[1] - y * BoxSizeY;
        ui_t tid = Maze(x, y);
        for (ui_t yy = 0; yy < SizeY; ++yy)
            for (ui_t xx = 0; xx < SizeX; ++xx) {
                bool notSameTeleport = !((xx == x) && (yy == y));
                if (notSameTeleport && tid == Maze(xx, yy))
                    return MakePoint(xx * BoxSizeX + dx, yy * BoxSizeY + dy);
            }
        return p;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline bool EqSign(float_t a, float_t b) const {
        return a * b >= 0.0;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline TPoint MakePoint(float_t x, float_t y) const {
        TPoint p(2);
        p[0] = x;
        p[1] = y;
        return p;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline float_t Dist(float_t x1, float_t y1, float_t x2, float_t y2) const {
        return norm_2(MakePoint(x1, y1) - MakePoint(x2, y2));
    }

    ////////////////////////////////////////////////////////////////////////////

    float_t Dist(const TPoint& point, const TAction& direction, 
    ui_t x, ui_t y) const {
        float_t dist = -1.0;

        float_t x0 = point[0];
        float_t y0 = point[1];

        float_t a = direction[0];
        float_t b = direction[1];

        float_t x1 = x * BoxSizeX;
        float_t y1 = y * BoxSizeY;
        float_t x2 = (x + 1) * BoxSizeX;
        float_t y2 = (y + 1) * BoxSizeY;

        float_t xx;
        float_t yy;
        float_t ll;

#define DIST_REQUIREMENTS(arg1, arg2, arg3) \
    EqSign(a, xx - x0) && EqSign(b, yy - y0) && \
    (arg1) >= (arg2) && (arg1) < (arg3) && (dist < 0.0 || ll < dist)

        if (a != 0) {
            xx = x1;
            yy = y0 + (b / a) * (xx - x0);
            ll = Dist(x0, y0, xx, yy);
            if (DIST_REQUIREMENTS(yy, y1, y2))
                dist = ll;

            xx = x2;
            yy = y0 + (b / a) * (xx - x0);
            ll = Dist(x0, y0, xx, yy);
            if (DIST_REQUIREMENTS(yy, y1, y2))
                dist = ll;
        }

        if (b != 0) {
            yy = y1;
            xx = x0 + (a / b) * (yy - y0);
            ll = Dist(x0, y0, xx, yy);
            if (DIST_REQUIREMENTS(xx, x1, x2))
                dist = ll;

            yy = y2;
            xx = x0 + (a / b) * (yy - y0);
            ll = Dist(x0, y0, xx, yy);
            if (DIST_REQUIREMENTS(xx, x1, x2))
                dist = ll;
        }
        return dist;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline float_t GetSensation(const TAction& direction) const {
        float_t minDist = MAX_FLOAT;
        for (size_t y = 0; y < Maze.size2(); ++y)
            for (size_t x = 0; x < Maze.size1(); ++x) {
                if (Maze(x, y) != 1) // TODO: Use consts!!!!
                    continue;
                float_t dist = Dist(Position, direction, x, y);
                if (dist >= 0.0 && dist < minDist)
                    minDist = dist;
            }
        return minDist;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void PushFood(float_t x, float_t y) {
        TPoint p(2);
        p[0] = x;
        p[1] = y;
        TFood food;
        food.Position = p;
        food.Counter = 0;
        Foods.push_back(food);
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void UpdateFoods() {
        for (ui_t i = 0; i < Foods.size(); ++i)
            if (Foods[i].Counter > 0)
                --Foods[i].Counter;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline TFood* FindFood(const TPoint& p) {
        for (ui_t i = 0; i < Foods.size(); ++i)
            if (norm_2(Foods[i].Position - p) <= FoodRadius)
                return &Foods[i];
        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void PutLine(const TPoint& p, const TAction& d, float_t l, Uint32 c) 
    {
        const float_t dl = 0.1;
        const TAction dd = dl * d / norm_2(d);
        TPoint pp = p;
        for (float_t ll = 0.0; ll < l; ll += dl, pp += dd) {
            Uint32 x = static_cast<Uint32>(pp[0]);
            Uint32 y = static_cast<Uint32>(pp[1]);
            NGraphics::PutPixel(x, y, c);
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    inline void PutLine(float_t x, float_t y, float_t xd, float_t yd, 
                        float_t l, Uint32 c) 
    {
        TPoint p(2);
        p[0] = x;
        p[1] = y;

        TAction a(2);
        a[0] = xd;
        a[1] = yd;

        PutLine(p, a, l, c);
    }

    ////////////////////////////////////////////////////////////////////////////

public:
    TMaze2D(ui_t sizeX,
            ui_t sizeY,
            float_t boxSizeX = 10.0,
            float_t boxSizeY = 10.0,
            float_t foodRadius = 4.0,
            ui_t foodCounter = 1000,
            ui_t storeSteps = 1000, 
            float_t sensSigma = 0.0) : // 0.1
    SizeX(sizeX),
    SizeY(sizeY),
    BoxSizeX(boxSizeX),
    BoxSizeY(boxSizeY),
    FoodRadius(foodRadius),
    FoodCounter(foodCounter),
    StoreSteps(storeSteps),
    SensSigma(sensSigma),
    Maze(sizeX, sizeY),
    Food(0.0)
    {
    }

    ////////////////////////////////////////////////////////////////////////////

    static TMaze2D* LoadMaze(const char* mazeFile) {
        std::ifstream ifs(mazeFile);
        const int ts = 1024;
        char tmp[ts];

        ui_t sizeX;
        ui_t sizeY;
        float_t boxSizeX;
        float_t boxSizeY;
        float_t foodRadius;
        float_t foodCounter;
        TPoint position(2);

#define LOAD_MAZE_CIN(cin_vars) ifs >> cin_vars; \
    ifs.getline(tmp, ts);

        LOAD_MAZE_CIN(sizeX >> sizeY);
        LOAD_MAZE_CIN(boxSizeX >> boxSizeY);
        LOAD_MAZE_CIN(foodRadius);
        LOAD_MAZE_CIN(foodCounter);
        LOAD_MAZE_CIN(position[0] >> position[1]);

        TMaze2D* maze = new TMaze2D(sizeX, sizeY, boxSizeX, boxSizeY,
            foodRadius, foodCounter);
        maze->Position = position;

        for (ui_t y = 0; y < sizeY; ++y)
            for (ui_t x = 0; x < sizeX; ++x) {
            ui_t box;
            ifs >> box;
            maze->Maze(x, y) = box;
        }

        while (1) {
            TPoint food(2);
            LOAD_MAZE_CIN(food[0] >> food[1]);
            if (ifs.fail())
                break;
            maze->PushFood(food[0], food[1]);
        }
        return maze;
    }

    ////////////////////////////////////////////////////////////////////////////

    virtual TSensation GetSensation() const {
        TSensation s(9);
        s[0] = GetSensation(MakePoint(+0.0, -1.0));
        s[1] = GetSensation(MakePoint(+1.0, -1.0));
        s[2] = GetSensation(MakePoint(+1.0, +0.0));
        s[3] = GetSensation(MakePoint(+1.0, +1.0));
        s[4] = GetSensation(MakePoint(+0.0, +1.0));
        s[5] = GetSensation(MakePoint(-1.0, +1.0));
        s[6] = GetSensation(MakePoint(-1.0, +0.0));
        s[7] = GetSensation(MakePoint(-1.0, -1.0));
        s = NormalVector(s, SensSigma);
        s[8] = Food;
        return s;
    }

    ////////////////////////////////////////////////////////////////////////////

    TAction RandomAction(float_t step) {
        TAction a(2);
        float_t alpha = Uniform<float_t>(0.0, 4.0);
        if (alpha >= 0.0 && alpha < 1.0) {
            a[0] = 0.0;
            a[1] = -1.0;
        } else if (alpha >= 1.0 && alpha < 2.0) {
            a[0] = 1.0;
            a[1] = 0.0;
        } else if (alpha >= 2.0 && alpha < 3.0) {
            a[0] = 0.0;
            a[1] = 1.0;
        } else if (alpha >= 3.0 && alpha < 4.0) {
            a[0] = -1.0;
            a[1] = 0.0;
        }
        return a * step;
    }

    ////////////////////////////////////////////////////////////////////////////

    virtual void ApplyAction(const TAction& action) {
        if (Food > 0.0)
            Food = 0.0;

        TFood* food = FindFood(Position);
        if (food && food->Counter == 0) {
            food->Counter = FoodCounter;
            Food = 1.0;
        }

        UpdateFoods();

        float_t step = norm_2(action);
        float_t dist = GetSensation(action);

        TAction a = action;
        while (dist <= step) { // TODO: JUST DO SOMETHING NO SO STUPID WITH IT!!!
            //return; // ?????????????????????????????????????????????
            a = RandomAction(step);
            dist = GetSensation(a);
        }
                
        TPoint p = Position + a;
        if (!IsTeleport(Position) && IsTeleport(p))
            Position = Teleportation(p);
        else
            Position = p;

        // TODO: Following code used only for visualization!!!
        Points.push_back(Position);
        while (Points.size() > StoreSteps)
            Points.pop_front();
    }

    ////////////////////////////////////////////////////////////////////////////

    virtual void Visualize() {
        const Uint32 K = 3;
        for (ui_t y = 0; y < SizeY; ++y) {
            for (ui_t x = 0; x < SizeX; ++x) {
                Uint32 at = Maze(x, y);
                NGraphics::Rect(x * BoxSizeX * K, y * BoxSizeY * K, 
                    BoxSizeX * K, BoxSizeY * K, at * 1000000);
            }
        }

        TPoints::const_iterator pit = Points.begin(); 
        for (; pit != Points.end(); ++pit)
            NGraphics::PutPixel((*pit)[0] * K, (*pit)[1] * K,
                NGraphics::clr_GREY_55);

        TSensation s = GetSensation();
        const TPoint& p = Position;
        PutLine(p[0] * K, p[1] * K, 0, -1, s[0] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, 1, -1, s[1] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, 1, 0, s[2] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, 1, 1, s[3] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, 0, 1, s[4] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, -1, 1, s[5] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, -1, 0, s[6] * K,NGraphics::clr_GREY_33);
        PutLine(p[0] * K, p[1] * K, -1, -1, s[7] * K,NGraphics::clr_GREY_33);

        for (ui_t k = 0; k < Foods.size(); ++k) {
            Uint32 c = NGraphics::clr_GREY_AA;
            if (Foods[k].Counter == 0)
                c = NGraphics::clr_GREEN;
            Uint32 R = static_cast<Uint32>(FoodRadius);
            NGraphics::Rect(Foods[k].Position[0] * K - R, 
                Foods[k].Position[1] * K - R, 2 * R, 2 * R, c);
        }

        NGraphics::Rect(Position[0] * K - 2, Position[1] * K - 2,
            5, 5, NGraphics::clr_RED);

    }
};

////////////////////////////////////////////////////////////////////////////////

}} // NABTFS::NEnvironment
