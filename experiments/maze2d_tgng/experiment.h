#include <environments/environments.h>
#include <animats/animats.h>
#include <common/common.h>

namespace NABTFS {
namespace N_MAZE2D_TGNG {

////////////////////////////////////////////////////////////////////////////////

using namespace NABTFS::NEnvironment;
using namespace NABTFS::NAnimat;
using namespace NABTFS::NInteractor;
using namespace NABTFS::NGraphics;
using namespace NABTFS;

////////////////////////////////////////////////////////////////////////////////

typedef TMaze2D TEnv;
typedef TGNG TAnm;
typedef TInteractor<TFloatVector, TFloatVector> TInt;

////////////////////////////////////////////////////////////////////////////////

void ProceedMaze(TAnm* animat, const char* maze, ui_t steps, ui_t stepsStep) {
    TEnv* mz = TEnv::LoadMaze(maze);
    TInt intr(mz, animat, stepsStep);
    for (ui_t i = 0; i < steps; ++i)
        intr.NextStep();
    delete mz;
}

////////////////////////////////////////////////////////////////////////////////

void DoExperiment(const char* maze_1, ui_t steps_1,
    const char* maze_2, ui_t steps_2,
    const char* maze_3, ui_t steps_3, ui_t stepsStep = 1)
{
    TAnm* anm = new TAnm;
    ProceedMaze(anm, maze_1, steps_1, stepsStep);
    ProceedMaze(anm, maze_2, steps_2, stepsStep);
    ProceedMaze(anm, maze_3, steps_3, stepsStep);
    delete anm;
}

}} // NABTFS::N_MAZE2D_TGNG
