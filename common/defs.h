#pragma once

////////////////////////////////////////////////////////////////////////////////

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/random.hpp>
#include <boost/math/constants/constants.hpp>
#include <math.h>
#include <limits>
#include <cstdlib>
#include <ctime>

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {

class TNotCopyable {
private:
    TNotCopyable(const TNotCopyable& obj) {
    }

    TNotCopyable& operator=(const TNotCopyable& obj) {
        return *this;
    }

public:
    TNotCopyable() {}
    virtual ~TNotCopyable() {}
};

////////////////////////////////////////////////////////////////////////////////

template <class T>
inline T Min(T a, T b) {
    return a < b ? a : b;
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
inline T Max(T a, T b) {
    return a > b ? a : b;
}

////////////////////////////////////////////////////////////////////////////////

typedef unsigned int ui_t;
typedef int i_t;
typedef double float_t;

typedef boost::numeric::ublas::vector<float_t> TFloatVector;
typedef boost::numeric::ublas::vector<ui_t> TUIntVector;
typedef boost::numeric::ublas::vector<i_t> TIntVector;
typedef boost::numeric::ublas::matrix<ui_t> TUIntMatrix;

////////////////////////////////////////////////////////////////////////////////

using boost::numeric::ublas::norm_2;
using boost::math::constants::pi;

////////////////////////////////////////////////////////////////////////////////

const float_t MAX_FLOAT = 1000000000.0;
const float_t PI = pi<float_t>();

////////////////////////////////////////////////////////////////////////////////

template <class T>
T Uniform(T a, T b) {
    static bool RandomInitialized = false;
    if (!RandomInitialized) {
        srand(static_cast<unsigned>(time(0)));
        RandomInitialized = true;
    }

    T result = a + (b - a) * static_cast<T>(rand()) / RAND_MAX;
    while (result == a)
        result = a + (b - a) * static_cast<T>(rand()) / RAND_MAX;
    return result;
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
T Normal(T mu, T sigm) {
    T U = Uniform<T>(static_cast<T>(0.0), static_cast<T>(1.0));
    T V = Uniform<T>(static_cast<T>(0.0), static_cast<T>(1.0));
    T N = sqrt(static_cast<T>(-2.0) * log(U)) * sin(static_cast<T>(2 * PI) * V);
    N = mu + N * sigm;
    return N;
}

////////////////////////////////////////////////////////////////////////////////

template <class V, class T>
V NormalVector(const V& v, T sigm) {
    V rv = v;
    for (ui_t i = 0; i < v.size(); ++i) {
        T UU = Uniform<T>(static_cast<T>(0.0), static_cast<T>(1.0));
        T VV = Uniform<T>(static_cast<T>(0.0), static_cast<T>(1.0));
        T N = sqrt(static_cast<T>(-2.0) * log(UU)) * sin(static_cast<T>(2 * PI) * VV);
        rv[i] = v[i] + N * sigm;
    }
    return rv;
}

////////////////////////////////////////////////////////////////////////////////

} // NABTFS
