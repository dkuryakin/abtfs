#pragma once

////////////////////////////////////////////////////////////////////////////////

#include "defs.h"

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {
namespace NEnvironment {

template <class TS, class TA>
class TBaseEnvironment : public TNotCopyable {
public:
    typedef TS TSensation;
    typedef TA TAction;
    typedef TBaseEnvironment<TSensation, TAction> TSelf;

    virtual TSensation GetSensation() const = 0;
    virtual void ApplyAction(const TAction& action) = 0;
    virtual void Visualize() = 0;
    virtual ~TBaseEnvironment() = 0;
};

template <class TS, class TA>
TBaseEnvironment<TS, TA>::~TBaseEnvironment() {}

}} // NABTFS::NEnvironment
