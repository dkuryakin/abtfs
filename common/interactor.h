#pragma once

////////////////////////////////////////////////////////////////////////////////

#include "base_environment.h"
#include "base_animat.h"
#include "graphics.h"

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {
namespace NInteractor {

////////////////////////////////////////////////////////////////////////////////

template <class TSensation, class TAction>
class TInteractor : public TNotCopyable {
public:
    typedef NABTFS::NEnvironment::TBaseEnvironment<TSensation, TAction> TEnvironment;
    typedef NABTFS::NAnimat::TBaseAnimat<TSensation, TAction> TAnimat;

private:
    ui_t Steps;

    TEnvironment* const Environment;
    TAnimat* const Animat;
    const ui_t VisualizeStep;

public:
    TInteractor(TEnvironment* environment, TAnimat* animat, ui_t visStep = 1);
    virtual ~TInteractor();

    void NextStep();
};

////////////////////////////////////////////////////////////////////////////////

template <class TSensation, class TAction>
TInteractor<TSensation, TAction>::TInteractor(TEnvironment* environment, TAnimat* animat, ui_t visStep) :
Steps(0),
Environment(environment),
Animat(animat),
VisualizeStep(visStep)
{
}

////////////////////////////////////////////////////////////////////////////////

template <class TSensation, class TAction>
TInteractor<TSensation, TAction>::~TInteractor() {
    //NABTFS::NGraphics::Close();
}

////////////////////////////////////////////////////////////////////////////////

template <class TSensation, class TAction>
void TInteractor<TSensation, TAction>::NextStep() {
    if (Steps == 0)
        NABTFS::NGraphics::FillScreen(NABTFS::NGraphics::clr_BLACK);

    TSensation sensation = Environment->GetSensation();
    Animat->ApplySensation(sensation);
    TAction action = Animat->GetAction();
    Environment->ApplyAction(action);

    if (Steps == 0) {
        Environment->Visualize();
        Animat->Visualize();
        NABTFS::NGraphics::Update();
    }

    ++Steps;
    if (Steps >= VisualizeStep)
        Steps = 0;
}

////////////////////////////////////////////////////////////////////////////////

}} // NABTFS::NInteractor
