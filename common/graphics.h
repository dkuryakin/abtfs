#pragma once

////////////////////////////////////////////////////////////////////////////////

#include <SDL/SDL.h>
#include <list>

#include "defs.h"

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {
namespace NGraphics {

////////////////////////////////////////////////////////////////////////////////

// Some predefined color constants.
const Uint32 clr_BLACK    = 0x000000;
const Uint32 clr_RED      = 0xFF0000;
const Uint32 clr_GREEN    = 0x00FF00;
const Uint32 clr_BLUE     = 0x0000FF;
const Uint32 clr_GREY_11  = 0x111111;
const Uint32 clr_GREY_22  = 0x222222;
const Uint32 clr_GREY_33  = 0x333333;
const Uint32 clr_GREY_44  = 0x444444;
const Uint32 clr_GREY_55  = 0x555555;
const Uint32 clr_GREY_66  = 0x666666;
const Uint32 clr_GREY_77  = 0x777777;
const Uint32 clr_GREY_88  = 0x888888;
const Uint32 clr_GREY_99  = 0x999999;
const Uint32 clr_GREY_AA  = 0xAAAAAA;
const Uint32 clr_GREY_BB  = 0xBBBBBB;
const Uint32 clr_GREY_CC  = 0xCCCCCC;
const Uint32 clr_GREY_DD  = 0xDDDDDD;
const Uint32 clr_GREY_EE  = 0xEEEEEE;
const Uint32 clr_WHITE    = 0xFFFFFF;

////////////////////////////////////////////////////////////////////////////////

// Interface for functions.
SDL_Surface* Init();
void Rect(Uint32 x, Uint32 y, Uint32 width, Uint32 height, Uint32 color = clr_BLACK);
void FillScreen(Uint32 color = clr_BLACK);
Uint32 Width();
Uint32 Height();
void Delay(Uint32 t = 0);
void Update();
void Close();

////////////////////////////////////////////////////////////////////////////////

inline void PutPixel(Uint32 x, Uint32 y, Uint32 color = clr_BLACK) {
    Rect(x, y, 1, 1, color);
}

////////////////////////////////////////////////////////////////////////////////

// Simple plotter.
template <class TX, class TY>
class TPlot : public TNotCopyable {
public:
    typedef TX TypeX;
    typedef TY TypeY;
    typedef std::list<TypeY> TPoints;

private:
    TPoints Values;
    const TypeX StepX;
    const TypeY MinY;
    const TypeY MaxY;
    const ui_t MaxPoints;
    const Uint32 X;
    const Uint32 Y;
    const Uint32 W;
    const Uint32 H;
    const Uint32 Color;

public:
    TPlot(TypeX stepX,
          TypeY minY,
          TypeY maxY,
          ui_t maxPoints = 1000,
          Uint32 x = 0,
          Uint32 y = 0,
          Uint32 w = 200,
          Uint32 h = 100,
          Uint32 color = clr_BLACK) : 
    StepX(stepX),
    MinY(minY),
    MaxY(maxY),
    MaxPoints(maxPoints),
    X(x),
    Y(y),
    W(w),
    H(h),
    Color(color)
    {}

    void PushPoint(TypeY value) {
        Values.push_back(value);
        while (Values.size() > MaxPoints)
            Values.pop_front();
    }

    void Plot() const {
        typename TPoints::const_iterator it = Values.begin();
        Rect(X, Y, 1, H, clr_WHITE);
        Rect(X, Y + H, W, 1, clr_WHITE);
        for (Uint32 cnt = 0; it != Values.end(); ++it, ++cnt) {
            if (*it < MinY || *it > MaxY)
                continue;
            Uint32 x = cnt * W / MaxPoints + X;
            TypeY ry = H * (*it - MinY) / (MaxY - MinY);
            Uint32 y = Y + H - static_cast<Uint32>(ry);
            PutPixel(x, y, Color);
        }
    }
};

////////////////////////////////////////////////////////////////////////////////

}} // NABTFS::NGraphics
