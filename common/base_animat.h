#pragma once

////////////////////////////////////////////////////////////////////////////////

#include "defs.h"

////////////////////////////////////////////////////////////////////////////////

namespace NABTFS {
namespace NAnimat {

template <class TS, class TA>
class TBaseAnimat : public TNotCopyable {
public:
    typedef TS TSensation;
    typedef TA TAction;
    typedef TBaseAnimat<TSensation, TAction> TSelf;

    virtual TAction GetAction() const = 0;
    virtual void ApplySensation(const TSensation& sensation) = 0;
    virtual void Visualize() = 0;
    virtual ~TBaseAnimat() = 0;
};

template <class TS, class TA>
TBaseAnimat<TS, TA>::~TBaseAnimat() {}

}} // NABTFS::NAnimat
